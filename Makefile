include Cargo.mk

all: $(OUT_DIR)/include.sh

$(OUT_DIR)/%: %.in | $(OUT_DIR)
	sed "s|@LIBDIR@|$(LIBDIR)|g" $< > $@

install:
	install -Dm755 $(OUT_DIR)/git-prompt "$(DESTDIR)$(LIBDIR)/zsh-git-prompt/prompt"
	install -Dm644 $(OUT_DIR)/include.sh "$(DESTDIR)$(DATADIR)/zsh/site-functions/_git_prompt_status"
