ifndef VERBOSE
.SILENT:
CARGO_VERBOSE_FLAG :=
else
CARGO_VERBOSE_FLAG := --verbose
endif

DESTDIR ?=

PREFIX ?= /usr
DATADIR ?= $(PREFIX)/share
LIBDIR ?= $(PREFIX)/lib

CARGO_BUILD_TARGET ?=
CARGO_TARGET_DIR ?= target
CARGO_PROFILE ?= release

ifeq ($(CARGO_PROFILE),dev)
OUT_DIR := $(CARGO_TARGET_DIR)/$(CARGO_BUILD_TARGET)/debug
else
OUT_DIR := $(CARGO_TARGET_DIR)/$(CARGO_BUILD_TARGET)/$(CARGO_PROFILE)
endif

all:
	cargo $(CARGO_VERBOSE_FLAG) build --profile $(CARGO_PROFILE) --locked

check:
	cargo $(CARGO_VERBOSE_FLAG) test --locked

fmt:
	cargo $(CARGO_VERBOSE_FLAG) fmt --all -- --check

install: all

clean:
	cargo $(CARGO_VERBOSE_FLAG) clean

$(OUT_DIR):
	mkdir -p $@

.PHONY: all check fmt install clean
