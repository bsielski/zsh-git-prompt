use std::io::{self, Read, Write};

#[cfg_attr(debug_assertions, derive(Debug))]
enum Oid {
    Initial,
    Commit(String),
}

impl Default for Oid {
    fn default() -> Self {
        Self::Initial
    }
}

impl From<&str> for Oid {
    fn from(s: &str) -> Self {
        match s {
            "(initial)" => Self::Initial,
            _ => Self::Commit(s.into()),
        }
    }
}

#[cfg_attr(debug_assertions, derive(Debug))]
enum Head {
    Detached,
    Branch(String),
}

impl Default for Head {
    fn default() -> Self {
        Self::Detached
    }
}

impl From<&str> for Head {
    fn from(s: &str) -> Self {
        match s {
            "(detached)" => Self::Detached,
            _ => Self::Branch(s.into()),
        }
    }
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Default)]
struct GitBranchUpstreamStatus {
    upstream: String,
    ahead: u32,
    behind: u32,
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Default)]
struct Ignored(String);

impl From<&str> for Ignored {
    fn from(s: &str) -> Self {
        Self(s.into())
    }
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Default)]
struct GitStatus {
    oid: Oid,
    head: Head,
    upstream: Option<GitBranchUpstreamStatus>,
    staged_count: u32,
    conflict_count: u32,
    changed_count: u32,
    untracked_count: u32,
    stash_count: u32,
}

impl GitStatus {
    fn parse_branch_upstream(&mut self, s: &str) {
        let status = self.upstream.get_or_insert(Default::default());
        status.upstream = s.into();
    }

    fn parse_branch_ahead(&mut self, s: &str) {
        let n = (s[1..]).parse().unwrap();
        let status = self.upstream.get_or_insert(Default::default());
        status.ahead = n;
    }

    fn parse_branch_behind(&mut self, s: &str) {
        let n = (s[1..]).parse().unwrap();
        let status = self.upstream.get_or_insert(Default::default());
        status.behind = n;
    }

    fn parse_line(&mut self, line: &str) {
        let mut line = line.split(' ');
        match line.next() {
            Some("#") => {
                match line.next() {
                    Some("branch.oid") => self.oid = Oid::from(line.next().unwrap()),
                    Some("branch.head") => self.head = Head::from(line.next().unwrap()),
                    Some("branch.upstream") => self.parse_branch_upstream(line.next().unwrap()),
                    Some("branch.ab") => {
                        self.parse_branch_ahead(line.next().unwrap());
                        self.parse_branch_behind(line.next().unwrap());
                    }
                    Some("stash") => self.stash_count = line.next().unwrap().parse().unwrap(),
                    _ => { /* Unknown Header */ }
                }
            }
            Some("1") | Some("2") => {
                let mut staged_unstaged = line.next().unwrap().chars();
                if staged_unstaged.next().unwrap() != '.' {
                    self.staged_count += 1;
                }
                if staged_unstaged.next().unwrap() != '.' {
                    self.changed_count += 1;
                }
            }
            Some("u") => self.conflict_count += 1,
            Some("?") => self.untracked_count += 1,
            Some("!") => {}
            _ => { /* Unknown Line */ }
        }
    }
}

const RESET_COLOR: &str = "%{\x1B[0m%}";

macro_rules! append_to_status {
    ( $( $x:expr ),* $(,)? ) => {
        {
            $(
                print!("{}", $x);
            )*
        }
    };
}

macro_rules! append_to_status_rst {
    ( $( $x:expr ),* $(,)? ) => {
        {
            append_to_status!($( $x, )* RESET_COLOR)
        }
    };
}

#[cfg_attr(debug_assertions, derive(Debug))]
struct Config {
    prefix: String,
    suffix: String,
    hash_prefix: String,
    separator: String,
    branch: String,
    staged: String,
    conflicts: String,
    changed: String,
    behind: String,
    ahead: String,
    stash: String,
    untracked: String,
    clean: String,
    local: String,
    upstream_front: String,
    upstream_end: String,
    merging: String,
    rebase: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            prefix: "[".into(),
            suffix: "]".into(),
            hash_prefix: ":".into(),
            separator: "|".into(),
            branch: "%{\x1B[1;35m%}".into(),
            staged: "%{\x1B[31m%}%{●%G%}".into(),
            conflicts: "%{\x1B[31m%}%{✖%G%}".into(),
            changed: "%{\x1B[34m%}%{✚%G%}".into(),
            behind: "%{↓·%2G%}".into(),
            ahead: "%{↑·%2G%}".into(),
            stash: "%{\x1B[1;34m%}%{⚑%G%}".into(),
            untracked: "%{\x1B[36m%}%{…%G%}".into(),
            clean: "%{\x1B[1;32m%}%{✔%G%}".into(),
            local: " L".into(),
            upstream_front: " {%{\x1B[34m%}".into(),
            upstream_end: "%{\x1B[0m%}}".into(),
            merging: "%{\x1B[1;35m%}|MERGING%{\x1B[0m%}".into(),
            rebase: "%{\x1B[1;35m%}|REBASE%{\x1B[0m%} ".into(),
        }
    }
}

impl Config {
    fn new() -> Self {
        let mut conf = Self::default();
        for (key, value) in std::env::vars_os() {
            let key = match key.into_string() {
                Ok(s) => s,
                Err(_) => continue,
            };
            let value = match value.into_string() {
                Ok(s) => s,
                Err(_) => continue,
            };

            match key.strip_prefix("ZSH_THEME_GIT_PROMPT_") {
                Some("PREFIX") => {
                    conf.prefix = value;
                }
                Some("SUFFIX") => {
                    conf.suffix = value;
                }
                Some("HASH_PREFIX") => {
                    conf.hash_prefix = value;
                }
                Some("SEPARATOR") => {
                    conf.separator = value;
                }
                Some("BRANCH") => {
                    conf.branch = value;
                }
                Some("STAGED") => {
                    conf.staged = value;
                }
                Some("CONFLICTS") => {
                    conf.conflicts = value;
                }
                Some("CHANGED") => {
                    conf.changed = value;
                }
                Some("BEHIND") => {
                    conf.behind = value;
                }
                Some("AHEAD") => {
                    conf.ahead = value;
                }
                Some("STASH") => {
                    conf.stash = value;
                }
                Some("UNTRACKED") => {
                    conf.untracked = value;
                }
                Some("CLEAN") => {
                    conf.clean = value;
                }
                Some("LOCAL") => {
                    conf.local = value;
                }
                Some("UPSTREAM_FRONT") => {
                    conf.upstream_front = value;
                }
                Some("UPSTREAM_END") => {
                    conf.upstream_end = value;
                }
                Some("MERGING") => {
                    conf.merging = value;
                }
                Some("REBASE") => {
                    conf.rebase = value;
                }
                _ => continue,
            }
        }
        conf
    }
}

fn main() {
    let input = {
        let mut input = Vec::new();
        if io::stdin().lock().read_to_end(&mut input).is_err() {
            return;
        }
        input
    };

    let mut skip_sep = false;
    let mut first_char = true;
    let splitter = |c: &u8| {
        if *c == b'\0' {
            if skip_sep {
                skip_sep = false;
                false
            } else {
                first_char = true;
                true
            }
        } else if first_char {
            first_char = false;
            skip_sep = *c == b'2';
            false
        } else {
            false
        }
    };

    let mut status = GitStatus::default();
    let mut git_repo = false;
    for line in input.split(splitter) {
        let line = match core::str::from_utf8(line) {
            Ok(line) => line,
            Err(_) => {
                return;
            }
        };
        if line.is_empty() {
            continue;
        }
        git_repo = true;
        status.parse_line(line);
    }

    if !git_repo {
        return;
    }

    let conf = Config::new();

    #[cfg(debug_assertions)]
    eprintln!("status: {:?}", status);
    #[cfg(debug_assertions)]
    eprintln!("config: {:?}", conf);

    append_to_status!(conf.prefix, conf.branch);
    match status.head {
        Head::Detached => match status.oid {
            Oid::Initial => append_to_status_rst!("(initial)"),
            Oid::Commit(ref commit) => append_to_status_rst!(conf.hash_prefix, &commit[..7]),
        },
        Head::Branch(ref branch) => append_to_status_rst!(branch),
    };

    let rebase = 0;
    if rebase != 0 {
        append_to_status_rst!(conf.separator, conf.rebase, rebase);
    }

    let merging = false;
    if merging {
        append_to_status_rst!(conf.separator, conf.merging);
    }

    status.upstream.as_ref().map_or_else(
        || {
            append_to_status_rst!(conf.local);
        },
        |upstream| {
            match status.head {
                Head::Detached => {}
                Head::Branch(ref branch) => {
                    if upstream.upstream.ends_with(branch) {
                        let position = upstream.upstream.len() - branch.len();
                        append_to_status_rst!(
                            conf.upstream_front,
                            &upstream.upstream[0..position],
                            ".",
                            conf.upstream_end
                        );
                    } else {
                        append_to_status_rst!(
                            conf.upstream_front,
                            upstream.upstream,
                            conf.upstream_end
                        );
                    }
                }
            };
            if upstream.behind != 0 {
                append_to_status_rst!(conf.behind, upstream.behind);
            }
            if upstream.ahead != 0 {
                append_to_status_rst!(conf.ahead, upstream.ahead);
            }
        },
    );
    append_to_status!(conf.separator);

    let mut clean = true;

    if status.staged_count != 0 {
        clean = false;
        append_to_status_rst!(conf.staged, status.staged_count);
    }

    if status.conflict_count != 0 {
        clean = false;
        append_to_status_rst!(conf.conflicts, status.conflict_count);
    }

    if status.changed_count != 0 {
        clean = false;
        append_to_status_rst!(conf.changed, status.changed_count);
    }

    if status.untracked_count != 0 {
        clean = false;
        append_to_status_rst!(conf.untracked, status.untracked_count);
    }

    if clean {
        append_to_status_rst!(conf.clean);
    }

    if status.stash_count != 0 {
        append_to_status_rst!(conf.stash, status.stash_count);
    }

    append_to_status_rst!(conf.suffix);

    println!();
    let _ = io::stdout().flush();
}
