Git prompt for zsh
==================
A ``zsh`` prompt which displays information about the current git repository.

This was heavily inspired by https://github.com/olivierverdier/zsh-git-prompt and
https://github.com/starcraftman/zsh-git-prompt.

Install
-------
Requirements: cargo and make installed on your system.

1. Clone this repository.
2. Run ``make && make install``.
3. Add the following code snippet to your zshrc: ::

    if ! autoload +X _git_prompt_status 2>/dev/null; then
        _git_prompt_status() {}
    fi

4. Configure your promt.
